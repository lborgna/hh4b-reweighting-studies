import pandas as pd
import numpy as np

import yaml
import argparse
import os

os.sys.path.append("../")

import uproot

from eda_tools.utilities import getNorm

from scipy.spatial.distance import euclidean

from hh4b_utils.nnt_tools import load_nnt

from eda_tools.bkg_helper import bs_error
from eda_tools.utilities import calculatedRhh

from scipy.stats import wasserstein_distance

import json

import logging

logging.basicConfig(level=logging.INFO)


def weighted_chisquare(
    f_obs, f_exp, f_obs_err, f_exp_err, ignore_stats=False, residuals=False, stat_lim=10
):
    # obs = 2b
    # exp = 4b

    from scipy.stats import chi2

    # Calculate weighted chi-square using method in arXiv:physics/0605123
    if ignore_stats == False:
        w1 = f_obs[(f_obs > stat_lim) | (f_exp > stat_lim)]
        w2 = f_exp[(f_obs > stat_lim) | (f_exp > stat_lim)]
        s1 = f_obs_err[(f_obs > stat_lim) | (f_exp > stat_lim)]  # noqa
        s2 = f_exp_err[(f_obs > stat_lim) | (f_exp > stat_lim)]  # noqa
    else:
        w1 = f_obs
        w2 = f_exp
        s1 = f_obs_err  # noqa
        s2 = f_exp_err  # noqa

    ndf = len(w1) - 1
    W1 = np.sum(w1)  # noqa
    W2 = np.sum(w2)  # noqa

    pi = (w1 * W1 / (s1 ** 2)) + (w2 * W2 / (s2 ** 2)) / (
        ((W1 ** 2) / (s1 ** 2)) + ((W2 ** 2) / (s2 ** 2))
    )

    R = (W1 * w2 - W2 * w1) ** 2 / (W1 ** 2 * s2 ** 2 + W2 ** 2 * s1 ** 2)
    R = np.where(((np.isnan(R)) | (np.isinf(R))), 0, R)
    # X2 = np.sum((W1*w2 - W2*w1)**2 / (W1**2 * s2**2 + W2**2 * s1**2))
    X2 = np.sum(R)
    p_value = chi2.sf(X2, ndf)
    if residuals:
        frac = 1 + ((W2 ** 2) * (s1 ** 2)) / ((W1 ** 2) / (s2 ** 2))
        top = w1 - W1 * pi
        bottom = s1 * np.sqrt(1 - 1 / frac)
        ri = top / bottom
        return (X2, p_value, ndf, X2 / ndf, ri)
    else:
        return (X2, p_value, ndf, X2 / ndf)


def shape_systematic(
    df: pd.DataFrame,
    feature: str,
    kr: int,
    bins=50,
    hrange=None,
    CR_weights: str = None,
    VR_weights: str = None,
) -> dict:

    mask_2b = df["ntag"] == 2
    mask_kr = df["kinematic_region"] == kr

    if CR_weights is None:
        CR_weights = df.filter(regex="NN_d24_weight_bstrap_med").columns.to_list()[0]

    if VR_weights is None:
        VR_weights = df.filter(
            regex="NN_d24_weight_VRderiv_bstrap_med"
        ).columns.to_list()[0]

    mu_cr = getNorm(df, k=2, weight_column=CR_weights)
    mu_vr = getNorm(df, k=1, weight_column=VR_weights)

    x = df.loc[mask_2b & mask_kr, feature].values
    w_cr = df.loc[mask_2b & mask_kr, CR_weights].values
    w_vr = df.loc[mask_2b & mask_kr, VR_weights].values

    cr_estimate, be = np.histogram(x, bins=bins, range=hrange, weights=mu_cr * w_cr)

    vr_estimate, _ = np.histogram(
        x,
        bins=be,
        range=hrange,
        weights=mu_vr * w_vr,
    )

    inv_estimate, _ = np.histogram(
        x, bins=be, range=hrange, weights=2 * mu_cr * w_cr - mu_vr * w_vr
    )
    results = {}
    results["cr_estimate"] = cr_estimate
    results["vr_estimate"] = vr_estimate
    results["inv_estimate"] = inv_estimate
    results["shape_err"] = np.abs(vr_estimate - cr_estimate)
    results["be"] = be

    return results


def histogram_4b(
    df: pd.DataFrame, feature: str, kinematic_region: int, bins=50, hrange: tuple = None
) -> dict:

    mask_kr = df["kinematic_region"] == kinematic_region
    mask_4b = df["ntag"] >= 4

    h_4b, be = np.histogram(
        df.loc[mask_kr & mask_4b, feature].values, bins=bins, range=hrange
    )
    h_4b_err = np.sqrt(h_4b)  # Poisson error (sqrt(N))
    _results = {"h_4b": h_4b, "h_4b_err": h_4b_err, "be_4b": be}
    return _results


def histogram_2b(
    df: pd.DataFrame,
    feature: str,
    kinematic_region: int,
    norm: float,
    weights_column: str = None,
    bins: int = 50,
    hrange: tuple = None,
) -> dict:

    mask_kr = df["kinematic_region"] == kinematic_region
    mask_2b = df["ntag"] == 2

    x = df.loc[mask_kr & mask_2b, feature].values
    if weights_column is not None:
        w = norm * df.loc[mask_kr & mask_2b, weights_column].values
    else:
        w = np.ones(x.shape[0])

    h_2b, be = np.histogram(x, bins=bins, range=hrange, weights=w)

    if weights_column is not None:
        # Poisson error of a weighted histogram
        h_2b_err_pois = np.histogram(x, bins=bins, weights=w ** 2)[0]
    else:
        h_2b_err_pois = np.sqrt(h_2b)

    _results = {"h_2b": h_2b, "h_2b_err_pois": h_2b_err_pois, "be_2b": be}
    return _results


def generate_histograms(
    df: pd.DataFrame,
    features: list,
    bin_edges: list,
    norm: float,
    kinematic_region: int,
    weights_column: str,
) -> dict:

    histos_2b = np.array([])
    histos_4b = np.array([])
    error_2b = np.array([])
    error_4b = np.array([])
    emds = np.array([])
    for i, (feature, be) in enumerate(zip(features, bin_edges)):

        r4b = histogram_4b(
            df, feature=feature, kinematic_region=kinematic_region, bins=be
        )
        histos_4b = np.append(histos_4b, r4b["h_4b"])
        error_4b = np.append(error_4b, r4b["h_4b_err"])

        r2b = histogram_2b(
            df,
            feature=feature,
            kinematic_region=kinematic_region,
            norm=norm,
            weights_column=weights_column,
            bins=be,
        )
        histos_2b = np.append(histos_2b, r2b["h_2b"])
        error_2b = np.append(error_2b, r2b["h_2b_err_pois"])

        emd = wasserstein_distance(r4b["h_4b"], r2b["h_2b"])
        emds = np.append(emds, emd)
    return {
        "histos_2b": histos_2b,
        "histos_2b_poi_err": error_2b,
        "histos_4b": histos_4b,
        "histos_4b_poi_err": error_4b,
        "emds": emds,
    }


def generate_bootstrap_errors(
    df: pd.DataFrame,
    features: list,
    bin_edges: list,
    kinematic_region: int,
    norm: float,
    norm_iqr: float,
    weights_column: str,
    weights_iqr: str,
) -> dict:

    bs_error_2b = np.array([])

    for i, (feature, be) in enumerate(zip(features, bin_edges)):
        bs_err = bs_error(
            df,
            kr=kinematic_region,
            be=be,
            mu_med=norm,
            mu_IQR=norm_iqr,
            W_med=weights_column,
            W_IQR=weights_iqr,
            feature=feature,
        )["h_err_bs_mag"]
        bs_error_2b = np.append(bs_error_2b, bs_err)

    return {"bs_err": bs_error_2b}


def generate_shape_systematic(
    df: pd.DataFrame,
    features: list,
    bin_edges: list,
    kinematic_region: int,
    weights_CR_column: str,
    weights_VR_column: str,
) -> dict:

    shape_syst_2b = np.array([])

    for i, (feature, be) in enumerate(zip(features, bin_edges)):
        shape_err = shape_systematic(
            df,
            feature=feature,
            kr=kinematic_region,
            bins=be,
            CR_weights=weights_CR_column,
            VR_weights=weights_VR_column,
        )["shape_err"]
        shape_syst_2b = np.append(shape_syst_2b, shape_err)
    return {"shape_err": shape_syst_2b}


def analyze_reweighting(
    df: pd.DataFrame,
    features: list,
    bin_edges: list,
    model_label: str = None,
    year: int = 16,
    verbose: bool = False,
) -> dict:

    labels = ["CRxCR", "VRxVR", "VRxCR", "CRxVR"]
    krs = [2, 1, 1, 2]
    model_region = ["CR", "VR", "CR", "VR"]

    chis2 = np.array([])
    chis2_bs = np.array([])
    chis2_shape = np.array([])

    emds = np.array([])
    if verbose:
        print("chis2: ", chis2)
        print("chis2: ", chis2_bs)
        print("chis2_shape: ", chis2_shape)
        print("emds: ", emds)

    h2b_all = np.array([])
    h4b_all = np.array([])
    error_2b_poi_all = np.array([])
    error_2b_bs_all = np.array([])
    error_2b_shape_all = np.array([])
    error_4b_poi_all = np.array([])

    for i, (l, kr, model_kr) in enumerate(zip(labels, krs, model_region)):
        if verbose:
            print(l)

        weight_cr_col = w_col(year, weight_label=model_label)
        weight_cr_iqr_col = w_iqr(year, weight_label=model_label)
        weight_vr_col = w_col(year, vr=True, weight_label=model_label)
        weight_vr_iqr_col = w_iqr(year, vr=True, weight_label=model_label)

        if model_kr == "CR":
            weight_column = weight_cr_col
            weights_iqr_col = weight_cr_iqr_col
            norm = get_norm(df, year, weight_label=model_label)
            mu_iqr = get_norm_iqr(df, year, weight_label=model_label)
            # norm = getNorm(df, k=2, weight_column=weight_column)
        elif model_kr == "VR":
            weight_column = weight_vr_col
            weights_iqr_col = weight_vr_iqr_col
            norm = get_norm(df, year, vr=True, weight_label=model_label)
            mu_iqr = get_norm_iqr(df, year=year, vr=True, weight_label=model_label)
            # norm = getNorm(df, k=1, weight_column=weight_column)

        histos = generate_histograms(
            df,
            features,
            bin_edges,
            norm,
            kinematic_region=kr,
            weights_column=weight_column,
        )
        if verbose:
            print("generated histos")
        bs_err = generate_bootstrap_errors(
            df,
            features,
            bin_edges,
            kinematic_region=kr,
            norm=norm,
            norm_iqr=mu_iqr,
            weights_column=weight_column,
            weights_iqr=weights_iqr_col,
        )
        if verbose:
            print("generated bs_error")
        shape_err = generate_shape_systematic(
            df,
            features,
            bin_edges,
            kinematic_region=kr,
            weights_CR_column=weight_cr_col,
            weights_VR_column=weight_vr_col,
        )
        if verbose:
            print("generated shape error")

        chi = weighted_chisquare(
            histos["histos_2b"],
            histos["histos_4b"],
            histos["histos_2b_poi_err"],
            histos["histos_4b_poi_err"],
        )[-1]

        chi_bs = weighted_chisquare(
            histos["histos_2b"],
            histos["histos_4b"],
            np.sqrt(histos["histos_2b_poi_err"] ** 2 + bs_err["bs_err"] ** 2),
            histos["histos_4b_poi_err"],
        )[-1]

        chi_shape = weighted_chisquare(
            histos["histos_2b"],
            histos["histos_4b"],
            np.sqrt(
                shape_err["shape_err"] ** 2
                + histos["histos_2b_poi_err"] ** 2
                + bs_err["bs_err"] ** 2
            ),
            histos["histos_4b_poi_err"],
        )[-1]

        h2b_all = np.append(h2b_all, histos["histos_2b"])
        h4b_all = np.append(h4b_all, histos["histos_4b"])
        error_2b_poi_all = np.append(error_2b_poi_all, histos["histos_2b_poi_err"])
        error_2b_bs_all = np.append(error_2b_bs_all, bs_err["bs_err"])
        error_2b_shape_all = np.append(error_2b_shape_all, shape_err["shape_err"])
        error_4b_poi_all = np.append(error_4b_poi_all, histos["histos_4b_poi_err"])

        emd_mean = histos["emds"].mean()
        emds = np.append(emds, emd_mean)

        chis2 = np.append(chis2, chi)
        chis2_bs = np.append(chis2_bs, chi_bs)
        chis2_shape = np.append(chis2_shape, chi_shape)

    ed = euclidean(chis2, np.ones(len(chis2)))
    ed_bs = euclidean(chis2_bs, np.ones(len(chis2_bs)))
    ed_shape = euclidean(chis2_shape, np.ones(len(chis2_shape)))
    ed_emd = euclidean(emds, np.zeros(len(emds)))

    chi2_all = weighted_chisquare(h2b_all, h4b_all, error_2b_poi_all, error_4b_poi_all)[
        -1
    ]
    chi2_bs_all = weighted_chisquare(
        h2b_all,
        h4b_all,
        np.sqrt(error_2b_bs_all ** 2 + error_2b_poi_all ** 2),
        error_4b_poi_all,
    )[-1]
    chi2_shape_all = weighted_chisquare(
        h2b_all,
        h4b_all,
        np.sqrt(error_2b_poi_all ** 2 + error_2b_bs_all ** 2 + error_2b_shape_all ** 2),
        error_4b_poi_all,
    )[-1]
    ### ONLY CR->CR and CR->VR results
    ### hack can't be assed atm
    be = np.linspace(200, 1200, 51)
    index_crcr = np.arange(0, be.shape[0] - 1)
    index_crvr = np.arange(3 * (be.shape[0] - 1), 4 * (be.shape[0] - 1))
    idx = np.append(index_crcr, index_crvr)
    chi2_2_all = weighted_chisquare(
        h2b_all[idx], h4b_all[idx], error_2b_poi_all[idx], error_4b_poi_all[idx]
    )[-1]
    chi2_2_bs_all = weighted_chisquare(
        h2b_all[idx],
        h4b_all[idx],
        np.sqrt(error_2b_bs_all[idx] ** 2 + error_2b_poi_all[idx] ** 2),
        error_4b_poi_all[idx],
    )[-1]
    chi2_2_shape_all = weighted_chisquare(
        h2b_all[idx],
        h4b_all[idx],
        np.sqrt(
            error_2b_poi_all[idx] ** 2
            + error_2b_bs_all[idx] ** 2
            + error_2b_shape_all[idx] ** 2
        ),
        error_4b_poi_all[idx],
    )[-1]

    ### ONLY CR->CR results
    idx = np.arange(0, be.shape[0] - 1)
    chi2_crcr_all = weighted_chisquare(
        h2b_all[idx], h4b_all[idx], error_2b_poi_all[idx], error_4b_poi_all[idx]
    )[-1]
    chi2_crcr_bs_all = weighted_chisquare(
        h2b_all[idx],
        h4b_all[idx],
        np.sqrt(error_2b_bs_all[idx] ** 2 + error_2b_poi_all[idx] ** 2),
        error_4b_poi_all[idx],
    )[-1]
    chi2_crcr_shape_all = weighted_chisquare(
        h2b_all[idx],
        h4b_all[idx],
        np.sqrt(
            error_2b_poi_all[idx] ** 2
            + error_2b_bs_all[idx] ** 2
            + error_2b_shape_all[idx] ** 2
        ),
        error_4b_poi_all[idx],
    )[-1]

    ### ONLY VR->VR results
    idx = np.arange(be.shape[0] - 1, 2 * (be.shape[0] - 1))
    chi2_vrvr_all = weighted_chisquare(
        h2b_all[idx], h4b_all[idx], error_2b_poi_all[idx], error_4b_poi_all[idx]
    )[-1]
    chi2_vrvr_bs_all = weighted_chisquare(
        h2b_all[idx],
        h4b_all[idx],
        np.sqrt(error_2b_bs_all[idx] ** 2 + error_2b_poi_all[idx] ** 2),
        error_4b_poi_all[idx],
    )[-1]
    chi2_vrvr_shape_all = weighted_chisquare(
        h2b_all[idx],
        h4b_all[idx],
        np.sqrt(
            error_2b_poi_all[idx] ** 2
            + error_2b_bs_all[idx] ** 2
            + error_2b_shape_all[idx] ** 2
        ),
        error_4b_poi_all[idx],
    )[-1]

    ### ONLY VR->CR results
    idx = np.arange(2 * (be.shape[0] - 1), 3 * (be.shape[0] - 1))
    chi2_vrcr_all = weighted_chisquare(
        h2b_all[idx], h4b_all[idx], error_2b_poi_all[idx], error_4b_poi_all[idx]
    )[-1]
    chi2_vrcr_bs_all = weighted_chisquare(
        h2b_all[idx],
        h4b_all[idx],
        np.sqrt(error_2b_bs_all[idx] ** 2 + error_2b_poi_all[idx] ** 2),
        error_4b_poi_all[idx],
    )[-1]
    chi2_vrcr_shape_all = weighted_chisquare(
        h2b_all[idx],
        h4b_all[idx],
        np.sqrt(
            error_2b_poi_all[idx] ** 2
            + error_2b_bs_all[idx] ** 2
            + error_2b_shape_all[idx] ** 2
        ),
        error_4b_poi_all[idx],
    )[-1]
    ### ONLY CR->VR results
    idx = np.arange(3 * (be.shape[0] - 1), 4 * (be.shape[0] - 1))
    chi2_crvr_all = weighted_chisquare(
        h2b_all[idx], h4b_all[idx], error_2b_poi_all[idx], error_4b_poi_all[idx]
    )[-1]
    chi2_crvr_bs_all = weighted_chisquare(
        h2b_all[idx],
        h4b_all[idx],
        np.sqrt(error_2b_bs_all[idx] ** 2 + error_2b_poi_all[idx] ** 2),
        error_4b_poi_all[idx],
    )[-1]
    chi2_crvr_shape_all = weighted_chisquare(
        h2b_all[idx],
        h4b_all[idx],
        np.sqrt(
            error_2b_poi_all[idx] ** 2
            + error_2b_bs_all[idx] ** 2
            + error_2b_shape_all[idx] ** 2
        ),
        error_4b_poi_all[idx],
    )[-1]
    ### Closure CR->CR results
    idx = np.arange(0, be.shape[0] - 1)
    chi2_closure = weighted_chisquare(
        h2b_all[idx], h4b_all[idx], error_2b_poi_all[idx], error_4b_poi_all[idx]
    )[-1]
    ### Extrapolation CR->VR results
    idx = np.arange(3 * (be.shape[0] - 1), 4 * (be.shape[0] - 1))
    chi2_extrapolation_bs = weighted_chisquare(
        h2b_all[idx],
        h4b_all[idx],
        np.sqrt(error_2b_poi_all[idx] ** 2 + error_2b_bs_all[idx] ** 2),
        error_4b_poi_all[idx],
    )[-1]
    chi2_extrapolation_shape = weighted_chisquare(
        h2b_all[idx],
        h4b_all[idx],
        np.sqrt(
            error_2b_poi_all[idx] ** 2
            + error_2b_bs_all[idx] ** 2
            + error_2b_shape_all[idx] ** 2
        ),
        error_4b_poi_all[idx],
    )[-1]

    results = {
        "chis2": chis2,
        "chis2_bs": chis2_bs,
        "chis2_shape": chis2_shape,
        "emds": emds,
        "ed": ed,
        "ed_bs": ed_bs,
        "ed_shape": ed_shape,
        "ed_emd": ed_emd,
        "chi2_all": chi2_all,
        "chi2_bs_all": chi2_bs_all,
        "chi2_shape_all": chi2_shape_all,
        "chi2_2_all": chi2_2_all,
        "chi2_2_bs_all": chi2_2_bs_all,
        "chi2_2_shape_all": chi2_2_shape_all,
        "chi2_crcr_all": chi2_crcr_all,
        "chi2_crcr_bs_all": chi2_crcr_bs_all,
        "chi2_crcr_shape_all": chi2_crcr_shape_all,
        "chi2_vrvr_all": chi2_vrvr_all,
        "chi2_vrvr_bs_all": chi2_vrvr_bs_all,
        "chi2_vrvr_shape_all": chi2_vrvr_shape_all,
        "chi2_vrcr_all": chi2_vrcr_all,
        "chi2_vrcr_bs_all": chi2_vrcr_bs_all,
        "chi2_vrcr_shape_all": chi2_vrcr_shape_all,
        "chi2_crvr_all": chi2_crvr_all,
        "chi2_crvr_bs_all": chi2_crvr_bs_all,
        "chi2_crvr_shape_all": chi2_crvr_shape_all,
        "chi2_closure": chi2_closure,
        "chi2_extrapolation_bs": chi2_extrapolation_bs,
        "chi2_extrapolation_shape": chi2_extrapolation_shape,
        "labels": labels,
    }
    return results


def w_col(year: int, vr: bool = False, weight_label: str = None) -> str:
    """returns the nominal weight column name"""
    vr_prefix = "_VRderiv" if vr else ""
    weight_prefix = f"_{weight_label}" if weight_label is not None else ""
    return f"NN_d24_weight{vr_prefix}{weight_prefix}_bstrap_med_{year}"


def w_iqr(year: int, vr: bool = False, weight_label: str = None) -> str:
    """returns the nominal weights iqr column name"""
    vr_prefix = "_VRderiv" if vr else ""
    weight_prefix = f"_{weight_label}" if weight_label is not None else ""
    return f"NN_d24_weight{vr_prefix}{weight_prefix}_bstrap_IQR_{year}"


"""
def get_mu_iqr(file, year: int = 16, vr: bool = False) -> float:
    get nominal iqr of norms value from NNT
    vr_fix = "_VRderiv" if vr else ""
    return file[f"NN_norm{vr_fix}_bstrap_IQR_{year}"]._fVal
"""


def get_norm(
    df: pd.DataFrame, year: int = 16, vr=False, weight_label: str = None
) -> float:
    vr_prefix = "_VRderiv" if vr else ""
    weight_prefix = f"_{weight_label}" if weight_label is not None else ""
    column = f"NN_norm{vr_prefix}{weight_prefix}_bstrap_med_{year}"
    norm = df[column][0]
    return norm


def get_norm_iqr(
    df: pd.DataFrame, year: int = 16, vr=False, weight_label: str = None
) -> float:
    vr_prefix = "_VRderiv" if vr else ""
    weight_prefix = f"_{weight_label}" if weight_label is not None else ""
    column = f"NN_norm{vr_prefix}{weight_prefix}_bstrap_IQR_{year}"
    norm = df[column][0]
    return norm


def load_yaml_to_dict(file_path: str) -> dict:
    """
    Loads yaml configuration file into dictionary
    """

    with open(file_path, "r") as f:
        return yaml.load(f, Loader=yaml.FullLoader)


def generate_bin_edges(features_config: dict) -> list:

    bin_edges = []
    for feature, config in features_config.items():
        be = np.linspace(config["range"][0], config["range"][1], config["nbins"] + 1)
        bin_edges.append(be)
    return bin_edges


def load_data_with_weights(
    data_file_path: pd.DataFrame, columns_to_load: list, weights_file_path: str
) -> pd.DataFrame:
    # load data
    df = load_nnt(data_file_path, columns=columns_to_load)
    logging.info(f"Event data loaded ! shape = {df.shape}")
    # load weights
    store = pd.HDFStore(weights_file_path)
    df_w = store["df"]
    logging.info(f"Weights loaded ! shape = {df_w.shape}")

    df = df.merge(
        df_w.drop(columns=["ntag", "kinematic_region", "pass_vbf_sel"]),
        on=["event_number", "run_number"],
        how="inner",
    )
    logging.info(f"Merged data and weights! shape = {df.shape}")
    if df.shape[0] != df_w.shape[0]:
        logging.warning(
            f"shape of weights file is different to data file, please be advised."
        )
    store.close()
    return df


def get_model_keys(weights_file_path: str) -> list:
    store = pd.HDFStore(weights_file_path)
    model_keys = store.get_storer("df").attrs.model_labels
    logging.info(f"Model keys: {model_keys}")
    store.close()
    return model_keys


def main(arguments):

    configuration = load_yaml_to_dict(arguments["config_file"])
    logging.info(f"loaded YAML configuration file {arguments['config_file']}")

    data_file = configuration["data_file"]["file_path"]
    year = configuration["data_file"]["year"]
    logging.info(f"Using data file {data_file}, with year {year}")
    weights_file = configuration["weights_file"]["file_path"]
    logging.info(f"Using weight file {weights_file}")
    output_file = configuration["output_file"]["file_path"]
    logging.info(f"results will be stored at {output_file}")

    features_config = configuration["features"]
    features_names = list(features_config.keys())
    logging.info(f"Using features {features_names}")
    bin_edges = generate_bin_edges(features_config)

    columns_to_load = [
        "ntag",
        "kinematic_region",
        "event_number",
        "run_number",
        "phi_h1",
        "phi_h2",
        "eta_h1",
        "eta_h2",
        "m_hh",
        "X_wt_tag",
        "pt_hh",
    ]
    df = load_data_with_weights(
        data_file_path=data_file,
        columns_to_load=columns_to_load,
        weights_file_path=weights_file,
    )
    calculatedRhh(df)
    print(df.head(2))
    print(df.columns)
    models_keys = get_model_keys(weights_file)
    # hack
    models_types = 23 * ["NN_nominal"] + 3 * ["NN_classifier"] + 14 * ["XGB"]
    bootstrap_only = arguments["bootstrap_only"]
    if not bootstrap_only:
        chis = {}
        chis_bs = {}
        chis_shape = {}
        ed = {}
        ed_bs = {}
        ed_shape = {}
        ed_emd = {}
        chis2_all = {}
        chis2_bs_all = {}
        chis2_shape_all = {}
        emds = {}
        chi2_2_all = {}
        chi2_2_bs_all = {}
        chi2_2_shape_all = {}
        chi2_crcr_all = {}
        chi2_crcr_bs_all = {}
        chi2_crcr_shape_all = {}
        chi2_vrvr_all = {}
        chi2_vrvr_bs_all = {}
        chi2_vrvr_shape_all = {}
        chi2_vrcr_all = {}
        chi2_vrcr_bs_all = {}
        chi2_vrcr_shape_all = {}
        chi2_crvr_all = {}
        chi2_crvr_bs_all = {}
        chi2_crvr_shape_all = {}
        chi2_closure = {}
        chi2_extrapolation_bs = {}
        chi2_extrapolation_shape = {}

        for i, model in enumerate(models_keys):

            logging.info(f"model number {i}, model key {model}")
            model_results = analyze_reweighting(
                df, features_names, bin_edges, model, verbose=False, year=year
            )

            chis[model] = model_results["chis2"]
            chis_bs[model] = model_results["chis2_bs"]
            chis_shape[model] = model_results["chis2_shape"]
            ed[model] = model_results["ed"]
            ed_bs[model] = model_results["ed_bs"]
            ed_shape[model] = model_results["ed_shape"]
            ed_emd[model] = model_results["ed_emd"]
            chis2_all[model] = model_results["chi2_all"]
            chis2_bs_all[model] = model_results["chi2_bs_all"]
            chis2_shape_all[model] = model_results["chi2_shape_all"]
            emds[model] = model_results["emds"]
            chi2_2_all[model] = model_results["chi2_2_all"]
            chi2_2_bs_all[model] = model_results["chi2_2_bs_all"]
            chi2_2_shape_all[model] = model_results["chi2_2_shape_all"]
            chi2_crcr_all[model] = model_results["chi2_crcr_all"]
            chi2_crcr_bs_all[model] = model_results["chi2_crcr_bs_all"]
            chi2_crcr_shape_all[model] = model_results["chi2_crcr_shape_all"]
            chi2_vrvr_all[model] = model_results["chi2_vrvr_all"]
            chi2_vrvr_bs_all[model] = model_results["chi2_vrvr_bs_all"]
            chi2_vrvr_shape_all[model] = model_results["chi2_vrvr_shape_all"]
            chi2_vrcr_all[model] = model_results["chi2_vrcr_all"]
            chi2_vrcr_bs_all[model] = model_results["chi2_vrcr_bs_all"]
            chi2_vrcr_shape_all[model] = model_results["chi2_vrcr_shape_all"]
            chi2_crvr_all[model] = model_results["chi2_crvr_all"]
            chi2_crvr_bs_all[model] = model_results["chi2_crvr_bs_all"]
            chi2_crvr_shape_all[model] = model_results["chi2_crvr_bs_all"]
            chi2_closure[model] = model_results["chi2_closure"]
            chi2_extrapolation_bs[model] = model_results["chi2_extrapolation_bs"]
            chi2_extrapolation_shape[model] = model_results["chi2_extrapolation_shape"]

            # print(model_results)

        opt_data = pd.DataFrame(
            {
                "model_key": models_keys,
                "model_type": models_types,
                "chis2_all": chis2_all.values(),
                "chis2_bs_all": chis2_bs_all.values(),
                "chis_shape_all": chis2_shape_all.values(),
                "chis": chis.values(),
                "chis_bs": chis_bs.values(),
                "chis_shape": chis_shape.values(),
                "ed": ed.values(),
                "ed_bs": ed_bs.values(),
                "ed_shape": ed_shape.values(),
                "ed_emd": ed_emd.values(),
                "chi2_2_all": chi2_2_all.values(),
                "chi2_2_bs_all": chi2_2_bs_all.values(),
                "chi2_2_shape_all": chi2_2_shape_all.values(),
                "chi2_crcr_all": chi2_crcr_all.values(),
                "chi2_crcr_bs_all": chi2_crcr_bs_all.values(),
                "chi2_crcr_shape_all": chi2_crcr_shape_all.values(),
                "chi2_vrvr_all": chi2_vrvr_all.values(),
                "chi2_vrvr_bs_all": chi2_vrvr_bs_all.values(),
                "chi2_vrvr_shape_all": chi2_vrvr_shape_all.values(),
                "chi2_crvr_all": chi2_crvr_all.values(),
                "chi2_crvr_bs_all": chi2_crvr_bs_all.values(),
                "chi2_crvr_shape_all": chi2_crvr_shape_all.values(),
                "chi2_vrcr_all": chi2_vrcr_all.values(),
                "chi2_vrcr_bs_all": chi2_vrcr_bs_all.values(),
                "chi2_vrcr_shape_all": chi2_vrcr_shape_all.values(),
                "chi2_closure": chi2_closure.values(),
                "chi2_extrapolation_bs": chi2_extrapolation_bs.values(),
                "chi2_extrapolation_shape": chi2_extrapolation_shape.values(),
            }
        )
        # print(opt_data.shape)
        # print(opt_data)
        # opt_data.to_json("dump.json")
        # opt_data.to_parquet("test_dump.parquet")
        opt_data.to_pickle(output_file + ".pkl")
        logging.info(f"Saved model results at {output_file} !")
    ### Bootstrap Errors
    model_bs_error = {}
    model_shape_error = {}
    histograms_mhh_2b = {}
    bs_perc_err_2b = {}
    shape_perc_err_2b = {}
    be = np.linspace(250, 1200, 51)
    x = 0.5 * (be[:-1] + be[1:])

    results_bootstraps = {}
    logging.info(f"Calculating boostraps errors in m_hh")
    for i, model in enumerate(models_keys):
        logging.info(f"model number {i}, model keys {model}")
        weight_col = w_col(year, weight_label=model)
        weight_iqr_col = w_iqr(year, weight_label=model)
        mu_med = get_norm(df, year, weight_label=model)
        mu_IQR = get_norm_iqr(df, year, weight_label=model)
        bs_err = bs_error(
            df,
            kr=2,
            be=be,
            W_med=weight_col,
            W_IQR=weight_iqr_col,
            mu_med=mu_med,
            mu_IQR=mu_IQR,
            feature="m_hh",
        )["h_err_bs_mag"]

        h_mhh_2b = histogram_2b(df, "m_hh", 2, mu_med, weight_col, be)

        shape_err = shape_systematic(
            df,
            "m_hh",
            2,
            be,
            CR_weights=w_col(year, weight_label=model),
            VR_weights=w_col(year, vr=True, weight_label=model),
        )["shape_err"]

        model_bs_error[model] = np.abs(bs_err)
        model_shape_error[model] = shape_err
        histograms_mhh_2b[model] = h_mhh_2b["h_2b"]
        bs_perc_err_2b[model] = 100 * np.abs(bs_err) / h_mhh_2b["h_2b"]
        shape_perc_err_2b[model] = 100 * shape_err / h_mhh_2b["h_2b"]

    results_bootstraps["model_bs_error"] = model_bs_error
    results_bootstraps["model_shape_error"] = model_shape_error
    results_bootstraps["histograms_mhh_2b"] = histograms_mhh_2b
    results_bootstraps["bs_perc_err_2b"] = bs_perc_err_2b
    results_bootstraps["shape_perc_err_2b"] = shape_perc_err_2b

    pd.DataFrame(results_bootstraps).to_pickle(output_file + "_bs.pkl")


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="This script will generate the minimal set of results of all the models that were optimized"
    )

    parser.add_argument(
        "-c",
        "--config",
        required=True,
        dest="config_file",
        type=str,
        help="YAML configuration file",
    )
    parser.add_argument(
        "-b", "--bootstrap-only", dest="bootstrap_only", action="store_true"
    )

    arguments = parser.parse_args()
    arguments = vars(arguments)

    main(arguments)